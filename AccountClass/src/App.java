public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("JRB1.50");

        Account TK1 = new Account("TK1", "Giang");
        Account TK2 = new Account("Tk2", "Linh", 1000);

        System.out.println(TK1.toString());
        System.out.println(TK2.toString());

        //Thêm tiền
        System.out.println("Thêm tiền");
        TK1.credit(2000);
        System.out.println(TK1);

        TK2.credit(3000);
        System.out.println(TK2);

        //trừ tiền
        System.out.println("Trừ tiền");
        TK1.debit(1000);
        System.out.println(TK1);

        TK2.debit(5000);
        System.out.println(TK2);

        
        //Chuyển tiền
        System.out.println("donate");
        TK1.transferTo(TK2, 2000);
        System.out.println(TK1);
        System.out.println(TK2);
        
        TK2.transferTo(TK1, 2000);
        System.out.println(TK1);
        System.out.println(TK2);

    }
}
