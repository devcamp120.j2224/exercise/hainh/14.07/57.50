public class Account {
    String id;
    String name;
    int balance = 0;
    //khởi tạo pt 2 tham số
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }
    //khởi tạo pt 3 tham số
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public int getBalance() {
        return balance;
    }


    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int credit(int amount) {
        balance += amount;
        return balance;
    }

    public int debit(int amount) {
        if(amount <= this.balance){
            balance -= amount;
        }else{
            System.out.println("Amount exceeded balance");
        }  
        return balance;
    } 

    // public int debit(int amount) {
    //     switch(balance){
    //         case 1 : 
    //         if( amount <= this.balance ){
    //             return  balance -= amount;
    //         }
    //         break;
    //         case 2 :

    //         if(amount != this.balance){
    //           System.out.println("dasdasdasdsadasdasdasdas");
    //           break;
    //         };  
    //     }    
    //     return balance;
    // }
    
    public int transferTo(Account another, int amount){
        // if(amount <= this.balance){
        //     another.balance += amount;
        // }else{
        //     System.out.println("Amount exceeded balance");
        // }
        // return balance;

        if (amount <= balance) {
            debit(amount);
            another.credit(amount);
            System.out.println("\nTransfer succesful. Tansfered: $" );
        } else if (amount > balance) {
            System.out.println("\nTransfer failed, not enough balance!");
        }
        return balance;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
    }

    
}
